import time
import os

# from fastapi import FastAPI, Request
# from fastapi.responses import HTMLResponse
# from fastapi.templating import Jinja2Templates

from flask import Flask
from flask import request
import redis

cache = redis.Redis(
    host="redis",
    port=6379,
)

# app = FastAPI(title="DS Lab 4")
app = Flask(__name__)

# templates = Jinja2Templates(directory="app/templates")

def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)

# @app.get("/", response_class=HTMLResponse)
@app.route("/")
def index():
    return f"I was seen {get_hit_count()} times."
    # return templates.TemplateResponse(
    #     "index.html", 
    #     {
    #         "request": request,
    #         "hit_cnt": get_hit_count(),
    #     },
    # )

# @app.get("/error")
@app.route("/error")
def error():
    shutdown_hook = request.environ.get('werkzeug.server.shutdown')
    if shutdown_hook is not None:
        shutdown_hook()
    return "Bye"


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80)
