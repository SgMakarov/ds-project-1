// ignore: avoid_web_libraries_in_flutter
import 'dart:convert';
import 'dart:html';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:web_app/components/my_outline_button.dart';
import 'package:web_app/components/section_title.dart';
import 'package:web_app/constants.dart';

const api_endpoint = const String.fromEnvironment("api_endpoint");

class ImageForm extends StatefulWidget {
  @override
  _ImageFormState createState() => _ImageFormState();
}

class _ImageFormState extends State<ImageForm> {
  Uint8List processedImageBinary;
  String repliedHost;
  String pongHost;
  bool _isLoading = false;

  _startFilePicker() async {
    InputElement uploadInput = FileUploadInputElement();
    uploadInput.accept = "image/*";
    uploadInput.click();

    uploadInput.onChange.listen((e) {
      // read file content as dataURL
      final files = uploadInput.files;
      if (files.length == 1) {
        final file = files[0];
        FileReader reader = FileReader();

        reader.onLoadEnd.listen((e) {
          setState(() {
            _isLoading = true;
            processedImageBinary = null;
          });

          sendImage(reader.result);
        });

        reader.readAsArrayBuffer(file);
      }
    });
  }

  void sendImage(Uint8List data) async {
    final b64 = Base64Encoder().convert(data);

    await http.post(
      api_endpoint,
      headers: {},
      body: {
        "image": b64,
      },
    ).then((response) {
      var body = json.decode(response.body);
      var host = body["host"];
      var image = Base64Decoder().convert(body["image"]);
      setState(() {
        _isLoading = false;
        processedImageBinary = image;
        repliedHost = host;
      });
    });
  }

  void downloadImage() async {
    final blob = Blob(processedImageBinary);
    final uri = Url.createObjectUrlFromBlob(blob);
    final anchor = document.createElement("a") as AnchorElement
      ..href = uri
      ..style.display = "none"
      ..download = "recognized.jpg";
    document.body.children.add(anchor);

    anchor.click();

    document.body.children.remove(anchor);
    Url.revokeObjectUrl(uri);
  }

  Widget getImagePlaceholder(BuildContext context) {
    if (_isLoading) {
      return CircularProgressIndicator();
    } else if (processedImageBinary != null) {
      return Image.memory(
        processedImageBinary,
        fit: BoxFit.fill,
      );
    } else {
      return Text("Please select an image");
    }
  }

  Widget getDownloadButton(BuildContext context) {
    if (processedImageBinary != null) {
      return Text("Answer is from $repliedHost");
//      return MyOutlineButton(
//        imageSrc: "assets/images/download.png",
//        text: "Download the result",
//        press: downloadImage,
//      );
    } else {
      return Container(
        width: 0,
        height: 0,
      );
    }
  }

  void pingAPI() async {
    setState(() {
      _isLoading = true;
    });
    await http
        .get(
      "$api_endpoint/ping",
    )
        .then((response) {
      var body = json.decode(response.body);
      setState(() {
        pongHost = body["host"];
        _isLoading = false;
      });
    });
  }

  Widget getPong(BuildContext context) {
    if (pongHost != null) {
      return Text("Pong by $pongHost");
    } else {
      return Container(
        width: 0,
        height: 0,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: kDefaultPadding,
      ),
      child: Column(
        children: [
          SectionTitle(
            color: Color(0xFFFF0000),
            title: "Service Offerings",
            subTitle: "We don't have much to show",
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ConstrainedBox(
                child: Center(
                  child: getImagePlaceholder(context),
                ),
                constraints: BoxConstraints.expand(
                  width: 512,
                  height: 512,
                ),
              ),
              Column(
                children: [
                  MyOutlineButton(
                    imageSrc: "assets/images/ping.png",
                    text: "Ping API",
                    press: pingAPI,
                  ),
                  getPong(context),
                  MyOutlineButton(
                    imageSrc: "assets/images/choose_img.png",
                    text: "Choose Image",
                    press: _startFilePicker,
                  ),
                  SizedBox(width: 10.0),
                  getDownloadButton(context),
                ],
              )
            ],
          ),
        ],
      ),
    );
  }
}
