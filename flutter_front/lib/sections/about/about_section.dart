import 'package:flutter/material.dart';
import 'package:web_app/constants.dart';

import 'components/about_section_text.dart';
import 'components/about_text_with_sign.dart';
import 'components/experience_card.dart';

class AboutSection extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: kDefaultPadding * 2),
      constraints: BoxConstraints(maxWidth: 1110),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              AboutTextWithSign(),
              Expanded(
                child: AboutSectionText(
                  text:
                      "We started as every other students with no idea of what"
                      " can we do. At one long evening we came up with idea "
                      "to apply our ML, IAI courses for the project and "
                      "googled pretty much every component since it doesn't "
                      "really matter for the assignment.",
                ),
              ),
              ExperienceCard(numOfExp: "3+"),
              Expanded(
                child: AboutSectionText(
                  text: "We are 3rd year student, so we are able to do "
                      "proper Googling and research StackOverflow. "
                      "By the way, we know something about virtualization "
                      "😏😏😏",
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
