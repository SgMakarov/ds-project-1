FROM cirrusci/flutter:latest as builder

ARG API_ENDPOINT

COPY . /build
WORKDIR /build
RUN sudo chmod -R 777 . \
    && rm -rf build \
    && flutter channel beta \
    && flutter upgrade \
    && flutter config --enable-web \ 
    && flutter build web --dart-define api_endpoint=$API_ENDPOINT


FROM python:3.7-alpine3.11 as deploy

COPY --from=builder /build/build/web /app
WORKDIR /app
ENTRYPOINT [ "python", "-m", "http.server", "8000" ]