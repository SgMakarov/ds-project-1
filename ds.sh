#!/usr/bin/env bash

# set -e; 
export DOCKER_COMPOSE_PATH="docker-compose.yml"
export DOCKERHUB_USERNAME="sgmakarov"

# install jq, it will come useful later
if ! command -v jq &> /dev/null
then
    echo "jq was not found. Installing for Debian/Ubuntu";
    echo "for other OS refer https://stedolan.github.io/jq/download/";
    sudo apt-get install jq;
fi


# Install docker-machine if not installed
if ! command -v docker-machine &> /dev/null
then
    echo "Docker-machine was not found. Installing for Linux.";
    echo "For other OS refer https://docs.docker.com/machine/install-machine/";
    # from docker-machine documentation:
    base=https://github.com/docker/machine/releases/download/v0.16.0 &&
      curl -L $base/docker-machine-$(uname -s)-$(uname -m) >/tmp/docker-machine &&
      sudo mv /tmp/docker-machine /usr/local/bin/docker-machine &&
      chmod +x /usr/local/bin/docker-machine;
fi



# create machines:
docker-machine create --driver virtualbox Master;
docker-machine create --driver virtualbox Worker;
docker-machine create --driver virtualbox Worker1;

docker-machine ls;
# NAME      ACTIVE   DRIVER       STATE     URL                         SWARM   DOCKER      ERRORS
# Master    -        virtualbox   Running   tcp://192.168.99.101:2376           v19.03.12   
# Worker    -        virtualbox   Running   tcp://192.168.99.102:2376           v19.03.12   
# Worker1   -        virtualbox   Running   tcp://192.168.99.103:2376           v19.03.12   

# Initialize swarm
export MASTER_IP=$(docker-machine inspect Master | jq -r '.Driver' | jq -r '.IPAddress')
docker-machine ssh Master docker swarm init --advertise-addr $MASTER_IP;

# Join workers to swarm
export JOIN_SWARM_WORKER=$(docker-machine ssh Master docker swarm join-token worker | grep 'docker swarm');
docker-machine ssh Worker $JOIN_SWARM_WORKER;
docker-machine ssh Worker1 $JOIN_SWARM_WORKER;


# Build and push into registry
cat ACCESS_TOKEN | docker login --username $DOCKERHUB_USERNAME --password-stdin
docker-compose build --no-cache
docker-compose push 

# Deploy project to swarm
eval $(docker-machine env Master);
docker stack deploy -c $DOCKER_COMPOSE_PATH --orchestrator swarm ds_project;

# setup monitoring
docker stack deploy --compose-file=portainer-agent-stack.yml portainer

# eval "$(docker-machine env -u)";


