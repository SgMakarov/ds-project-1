#!/usr/bin/env bash

eval $(docker-machine env Master);
docker stack rm ds_project;
eval "$(docker-machine env -u)";
docker-machine rm Master Worker Worker1 -y
unset DOCKER_COMPOSE_PATH DOCKERHUB_USERNAME MASTER_IP JOIN_SWARM_WORKER;