import base64
import socket

from flask import Flask, request
from flask_cors import CORS
from helpers import process_image

app = Flask(__name__)
CORS(app)


def add_host(data: dict) -> dict:
    data.update(
        {
            "host": socket.gethostbyname(socket.gethostname()),
        }
    )
    return data


@app.route('/', methods=['POST'])
def upload_file():
    file = request.form['image']
    file = base64.b64decode(file)
    if file:
        return add_host({
            "image": process_image(file),
        })


@app.route("/ping", methods=["GET"])
def ping():
    return add_host({
        "pong": True,
    })


if __name__ == '__main__':
    app.run("0.0.0.0", 5000)
